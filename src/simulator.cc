#include <cstdio>
#include <cstring>
#include <ctime>
#include <cassert>
#include <iostream>
#include "simulator.h"

using namespace std;

#define CHECK_PARAM(arg, enable) \
  if(!strcmp(arg, (argv)[i])) { \
    enable = true;  \
  } 

// Static member definition
char *Simulator::conf_filepath_  = NULL;
char *Simulator::sweep_filepath_ = NULL;
bool Simulator::simulate_ = false;

Simulator::Simulator(void)
{
  conf_filepath_  = new char[MAX_FILEPATH_LEN];
  sweep_filepath_ = new char[MAX_FILEPATH_LEN];
  legionDAG_ = new LegionDAG();
}

Simulator::Simulator(int argc, char *argv[])
{
  conf_filepath_  = new char[MAX_FILEPATH_LEN];
  sweep_filepath_ = new char[MAX_FILEPATH_LEN];
  legionDAG_ = new LegionDAG();
  Init(argc, argv);
}

Simulator::~Simulator(void)
{
  delete []conf_filepath_;
  delete []sweep_filepath_;
}

void Simulator::Init(int argc, char *argv[])
{
  printf("./sim -dag {DAG file} -s {sweep file} -est -sim\n");
  for(int i=1; i<argc; i++) {
    if(!strcmp("-dag", (argv)[i])) {
       PRINT("argv : %s\n", (argv)[i]);
       strcpy(conf_filepath_, argv[++i]);
       cout << "filename : " << conf_filepath_ << endl; 
    }
  }

  cout << "flag check : " << conf_filepath_ << " "
                          << sweep_filepath_ << " "
                          << simulate_ 
                          << endl;

}

bool Simulator::LoadConfig(void)
{
  PRINT("Simulator::LoadConfig()\n");

  legionDAG_->CreateDAG(conf_filepath_);
  legionDAG_->SetIter();
  legionDAG_->InitSweepID();
  legionDAG_->Init();

  return true;
}

void Simulator::Run(void)
{
  PRINT("\nSimulator::Run() - %d\n", simulate_);
  legionDAG_->TraverseDAG(&LegionDAG::PrintTime);

  if(simulate_) {
    legionDAG_->RunSimulation();
  }
  else {
    legionDAG_->RunEstimation();
  }

}

void Simulator::Sweep(void)
{
//  legionDAG_->InitSweepID();
//  legionDAG_->Sweep();
};

void Simulator::DumpResult(void)
{
  PRINT("Simulator::DumpResult()\n");

//  legionDAG_->TraverseDAG(&LegionDAG::PrintInfo);

  char outputfilename[32] = "./output.cvs";
  legionDAG_->DumpResult(outputfilename);

}


int main(int argc, char *argv[])
{
  printf("Simulator for resilient Legion programming system with Containment Domains.\n\n");
//  int loop_count = 1;
  // Create Simulator object
  Simulator *simulator = new Simulator(argc, argv);

  // Load configuration
  if( simulator->LoadConfig() ) {

//    for(int i=0; i<loop_count; i++) {
//       
//      simulator->Sweep(); 
      // Do simulation
      simulator->Run();
  
//    }
  
    // Dumping results and finalize simulation 
    simulator->DumpResult();

  }
  else {
    ERROR("Simulator::LoadConfig() failed.\n");
  }
  

  return 0;

}
