#include <cstdlib>
#include <string>
#include <vector>
#include "legion_dag.h"
#include "define.h"

class Simulator {
  static char *conf_filepath_;
  static char *sweep_filepath_;
  static bool simulate_;
  LegionDAG   *legionDAG_;  
public:
  Simulator(void);
  Simulator(int argc, char *argv[]);
  ~Simulator(void);
  void Init(int argc, char *argv[]);
  bool LoadConfig(void);
  void Sweep(void);
  void Run(void);
  void DumpResult(void);
};
