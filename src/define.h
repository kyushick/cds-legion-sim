#include <cstdint>
#include <cassert>

#define DOUBLE    double
#define FLOAT     float
#define FLOAT32   float
#define FLOAT64   double
#define FLOAT128  long double
#define UINT64    uint64_t
#define UINT32    uint32_t
#define INT64     int64_t
#define INT64     int64_t

#define KILO      1024
#define MEGA      1048576
#define GIGA      1073741824

#define MAX_FAULTS 10
#define MAX_FILEPATH_LEN 256
#define MAX_LINEBUFF_SIZE 2048
#define ERROR(...) \
  { fprintf(stderr, __VA_ARGS__); assert(0); }


#if VERBOSE == 0  // No printouts -----------

#define PRINT(...) 
 
#elif VERBOSE == 1  // print to stdout ------

#define PRINT(...) \
  fprintf(stdout, __VA_ARGS__)

#elif VERBOSE == 2  // Print to fileout -----

extern FILE *fout;

#define PRINT(...) \
  fprintf(fout, __VA_ARGS__)

#else  // -------------------------------------

#define PRINT(...) \
  fprintf(stderr, __VA_ARGS__)

#endif
