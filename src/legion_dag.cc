#include <cmath>
#include <cstring>
#include "legion_dag.h"

using namespace std;

#define MaskPreserveFlag(iter,sweep_mask,sweep_id) \
  (iter & sweep_mask) >> sweep_id
/************************************** Legion DAG *****************************************/
unsigned long LegionDAG::sweep_id_gen_ = 0;
unsigned long LegionDAG::curr_iter_ = 0;
unsigned long LegionDAG::curr_prv_num_ = 0;
map<unsigned long, unsigned long> LegionDAG::pred_counter_;
list<TaskNode *> LegionDAG::topo_list_;

LegionDAG::LegionDAG(void) 
{
  sweep_id_gen_ = 0;
  PRINT("LegionDAG Created\n");

}  

LegionDAG::~LegionDAG(void) 
{
  PRINT("LegionDAG Destroyed\n");

}  

// Create from DAG info file
void LegionDAG::CreateDAG(const char *filename) 
{
  PRINT("LegionDAG::CreateDAG(%s)\n", filename);

#ifndef NO_FILE_INPUT
  cout << "filename : " << filename << endl;
  // Read file to build DAG
  FILE *fp = fopen(filename, "r");
//  char *pFile = NULL;
  char *lineBuf = new char[MAX_LINEBUFF_SIZE];
  char *pTok = NULL;
  if(fp != NULL) {
    //int count=0;
    while( !feof(fp) ) {
      cout << "================================="<< endl;
      fgets(lineBuf, MAX_LINEBUFF_SIZE, fp);

      pTok = strtok(lineBuf, ":");
      cout << "pTok 1 : " << pTok << endl;

      unsigned long curr_node_id = stoul(pTok);
      TaskNode *curr_node = NULL;
      if(task_map_.find(curr_node_id) == task_map_.end()) { // Check if the task is registered
        // Root task node
        if(task_map_.empty()) {
          // Inserting initial node
          curr_node = new TaskNode(curr_node_id);
          task_map_[curr_node_id] = curr_node;
          root_ = curr_node;
        }
        else {
          curr_node = new TaskNode(curr_node_id, sweep_id_gen_++);
          task_map_[curr_node_id] = curr_node;
        }
      }
      else {
        curr_node = task_map_[curr_node_id];
      }

      cout << "----------------------------"<< endl;

      while(pTok != NULL) { // Parsing children list of TaskNode
        pTok = strtok(NULL, ":");
        if(pTok != NULL && (int)(*pTok) != 10) {
//          cout << "token [:] : " << strcmp(pTok, "")<< " | "  <<strcmp(pTok, " ")<<" | "<< strcmp(pTok, "\0")<< endl;
          
          uint32_t child_node_id = stoul(pTok);
          TaskNode *child_node = NULL;
          if(task_map_.find(child_node_id) == task_map_.end()) {
            child_node = new TaskNode(child_node_id, sweep_id_gen_++);
            // If it is not registered to map, register it now.
            task_map_[child_node_id] = child_node;
            cout <<"child node id " <<child_node_id << " is created"<<endl;
          }
          else {
            child_node = task_map_[child_node_id];
            cout <<"child node id " <<child_node_id << endl;
          }
          curr_node->AddChildNode(child_node);

        }
        else break;
      }

      pTok = strtok(lineBuf, "=");
      cout << "\npTok 2 : " << pTok << endl;
      uint32_t target_node_id = stoul(pTok);
      if(task_map_.find(target_node_id) == task_map_.end()) {
        // If it is not registered to map, register it now.
        task_map_[target_node_id] = new TaskNode(target_node_id, sweep_id_gen_++);
      }

      while(pTok != NULL) { // Parsing children list of TaskNode
        pTok = strtok(NULL, "=");
        if(pTok != NULL) {
          cout << "token [=] : " << pTok << endl;
         
          double execution_time = atof(pTok);
          cout << "double : " << execution_time << endl;
          task_map_[target_node_id]->SetExecTime(execution_time);


        } 
        else break;
      }
    }


  }
  else {
    ERROR("ERROR: Reading file for DAG info is failed.\n");
  }

  
#else


  PRINT("No filepath passed for DAG. Create default DAG\n");
  

#endif
  cout << "WHAT? \n"<< endl;
}

// Perform Breadth-First Search from root node of DAG
void LegionDAG::RunEstimation(void)
{
  PRINT("LegionDAG::RunEstimation()");
  for(curr_iter_=0; curr_iter_<iter_; curr_iter_++) {
    ResultObj *result = new ResultObj();
    // reset prv num
    curr_prv_num_ = 0;


#ifdef SWEEP
    TraverseDAG(&LegionDAG::SetSweepMask);
#endif

    root_->is_preserved_ = true; // Root should always true
    TraverseDAG(&LegionDAG::Estimate);
    TraverseDAG(&LegionDAG::PrintInfo);

    TopologicalSort();

    RecordResult(result);
    sweep_map_[curr_iter_] = result;
    prv_num_map_[curr_prv_num_].push_back(result);
  }
}

void LegionDAG::InitSweepID(void)
{
  sweep_id_gen_ = 0;
  TraverseDAG(&LegionDAG::SetSweepID);
}

void LegionDAG::Init(void)
{
  TraverseDAG(&LegionDAG::CalcNodeInfo);
}

void LegionDAG::RecordResult(ResultObj *result)
{
  double energy = 0.0;
  double time = 0.0;

  for(auto it=task_map_.begin(); it!=task_map_.end(); ++it) {
    if(it->second->expected_exec_time_ > time){
      time = it->second->expected_exec_time_;
    }
    energy += it->second->effective_expected_energy_;
  }

  result->time_ = time;
  result->energy_ = energy;

  for(auto it=topo_list_.begin(); it!=topo_list_.end(); ++it) {
    result->topo_map_[(*it)->uid_] = (*it)->expected_exec_time_;
  }
}

void LegionDAG::DumpResult(const char *filepath)
{

  FILE *outfp = fopen(filepath, "w+");

  pair<unsigned long, double> min_time = make_pair(0,9999999999999.0);
  pair<unsigned long, double> min_energy = make_pair(0,9999999999999.0);
  for(auto it=sweep_map_.begin(); it!=sweep_map_.end(); ++it) {
    if(it->second->time_ < min_time.second) {
      min_time.first = it->first;
      min_time.second = it->second->time_;
    }
  }

  for(auto it=sweep_map_.begin(); it!=sweep_map_.end(); ++it) {
    if(it->second->energy_ < min_energy.second) {
      min_energy.first = it->first;
      min_energy.second = it->second->energy_;
    }
  }

  fprintf(outfp, "%lu %f\n%lu %f\n", min_time.first, min_time.second, min_energy.first, min_energy.second);

  for(auto it=sweep_map_.begin(); it!=sweep_map_.end(); ++it) {
    fprintf(outfp, "%lu %f %f\n", it->first, it->second->time_, it->second->energy_);
  }
  fprintf(outfp, "\n\n");
  for(auto it=prv_num_map_.begin(); it!=prv_num_map_.end(); ++it) {
//    fprintf(outfp, "%lu", it->first);
    for(auto jt=it->second.begin(); jt!=it->second.end(); ++jt) {
      fprintf(outfp, "%lu %f %f\n", it->first, (*jt)->time_, (*jt)->energy_);
    }
    fprintf(outfp, "\n");
  }

  fprintf(outfp, "\n\n");
  
  for(auto it=sweep_map_.begin(); it!=sweep_map_.end(); ++it) {
    fprintf(outfp, "%lu\n", it->first);
    for(auto jt=it->second->topo_map_.begin(); jt!=it->second->topo_map_.end(); ++jt) {
      fprintf(outfp, "%lu %f\n", jt->first, jt->second);
    }
    fprintf(outfp, "\n");
  }
}

void LegionDAG::CalcNodeInfo(TaskNode *target)
{
  // FIXME
  target->preserve_time_ = target->exec_time_ * 0.1;
  target->restore_time_  = target->exec_time_ * 0.1;
  target->preserve_energy_ = 0.0;
  target->restore_energy_  = 0.0;
  //target->failure_prob_    = pow(2.7, -1 * target->exec_time_ * target->failure_rate_);
  target->failure_prob_    = 0.0001;
  target->energy_ = 1.0 * target->exec_time_;


  //pred_counter_[target->uid_] = target->parent_list_.size();

}


void LegionDAG::SetSweepID(TaskNode *target)
{
  if(!target->parent_list_.empty()) {
    target->sweep_id_ = sweep_id_gen_++;
    target->sweep_mask_ = pow(2, target->sweep_id_);
  }
  else {
    target->sweep_id_ = -1;
    target->sweep_mask_ = 0;
  }
}

void LegionDAG::SetSweepMask(TaskNode *target)
{
  target->is_preserved_ = MaskPreserveFlag(curr_iter_, target->sweep_mask_, target->sweep_id_);
  curr_prv_num_ += (int)target->is_preserved_;
}

// Perform Breadth-First Search from root node of DAG
void LegionDAG::RunSimulation(void)
{
  PRINT("LegionDAG::RunSimulation()\n");


}

//void LegionDAG::SetPreserve(map<unsigned long, TaskNode *>::iterator target)
//{
//
//  for(auto it=task_map_.begin(); it!=task_map_.end(); ++it) {
//
//    if(it->uid_ == target->uid_)
//      it->is_preserved_ = true;
//    else
//      it->is_preserved_ = false;
//
//  }
//
//}

void LegionDAG::SetIter(void)
{
#ifdef SWEEP
  iter_ = pow(2, task_map_.size() - 1);
#else
  iter_ = 1;
#endif

  PRINT("Total iteration : %lu\n", iter_);
}

void LegionDAG::Estimate(TaskNode *target)
{
  PRINT("LegionDAG::Estimate()\n");

  target->GetExpectedExecTime();
  target->GetExpectedEnergy();

}


#define PRINT_TIME(...) \
  fprintf(stdout, __VA_ARGS__)

void LegionDAG::PrintTime(TaskNode *target)
{
  //PRINT("LegionDAG::PrintTime()\n");
  cout << "["<<target->uid() << "] exec time : " << target->exec_time() << endl;

}


void LegionDAG::PrintEnergy(TaskNode *target)
{
//  PRINT("LegionDAG::PrintEnergy()\n");
  cout << "["<<target->uid() << "] energy : " << target->energy() << endl;


}

void LegionDAG::PrintInfo(TaskNode *target)
{
  target->PrintInfo();
}


void LegionDAG::TraverseDAG(const function<void(TaskNode*)> &Op) 
{
  PRINT("LegionDAG::TraverseDAG()\n");
  list<TaskNode*> queue;
  map<unsigned long, bool> visited;
  
  // Initialize visited flag.
  for(auto it=task_map_.begin(); it!=task_map_.end(); ++it) {
    visited[it->first] = false;
  }
 
  queue.push_back(root_);
  visited[root_->uid()] = true;
 
  while( !queue.empty() ) {
    TaskNode *target = queue.front();
    queue.pop_front();
    //Do Something
    Op(target);
//    cout << "\n["<<target->uid() << "]  : is traversing" << endl << endl;
    for(auto jt=target->children_list_.begin(); 
             jt!=target->children_list_.end(); ++jt) {
//      cout << "[" << (*jt)->uid()<<"] visited? " << visited[(*jt)->uid()] << endl;
      if(visited[(*jt)->uid()] == false)  {
        visited[(*jt)->uid()] = true;
//        cout << "node #"<<(*jt)->uid() << " is pushed back" << endl;
        queue.push_back(*jt);
      }
    }
  }
  

}

void LegionDAG::TopologicalSort() 
{
  // Initialize erased flage for the next iteration
  for(auto it=task_map_.begin(); it!=task_map_.end(); ++it) {
    it->second->erased_ = false;
    pred_counter_[it->first] = it->second->parent_list_.size();
  }

  topo_list_.clear();

  root_->erased_ = true;
  topo_list_.push_back(root_);
  TopologicalSort(root_); 
  
  PRINT("==Topological Sort==========================\n");
  for(auto it=topo_list_.begin(); it!=topo_list_.end(); ++it) {
    PRINT("[%lu] exec time : %f \n", (*it)->uid_, (*it)->expected_exec_time_);
  } 
  PRINT("===========================================\n");
}

void LegionDAG::TopologicalSort(TaskNode *target) 
{
  list<TaskNode *> next_target_list;
  next_target_list.clear();

  // erase target node from topo list that has no incoming edge
  // go to successor node and decrease pred_counter by 1 for each
  double max_expected_exec_time = 0.0;
  TaskNode *next_target = NULL;
  for(auto it=target->children_list_.begin(); it!=target->children_list_.end(); ++it) {
    if((*it)->erased_ == false) {
      pred_counter_[(*it)->uid_]--;
    }

    // Among the same topological-order nodes, print the most long-execution one.
    if(pred_counter_[(*it)->uid_] == 0) {
      (*it)->erased_ = true;
      if((*it)->expected_exec_time_ > max_expected_exec_time) {
        max_expected_exec_time = (*it)->expected_exec_time_;
        next_target = *it;
      }
      next_target_list.push_back(*it);
    }
  }
  
  // if the pred_counter is 0, insert to the topo list
  if(next_target == NULL) return;
  else {
    topo_list_.push_back(next_target);
    for(auto it=next_target_list.begin(); it!=next_target_list.end(); ++it) {
      TopologicalSort(*it);
    }
  }
}



/************************************** Task Node *****************************************/
random_device TaskNode::generator_;

TaskNode::TaskNode(const unsigned long &uid) 
  : distribution_(0.0, 1.0) 
{
  uid_ = uid;
  sweep_id_ = 0; 
  failure_rate_ = 0.300000001;
  is_preserved_ = true;
  energy_ = 1.0; 
  data_volume_ = 1.0;
  erased_ = false; 
}

TaskNode::TaskNode(const unsigned long &uid, const long &sweep_id) 
  : distribution_(0.0, 1.0) 
{
  uid_ = uid;
  sweep_id_ = sweep_id; 
  failure_rate_ = 0.300000001;
  is_preserved_ = true; 
  energy_ = 1.0;
  data_volume_ = 1.0; 
  erased_ = false; 
}

TaskNode::~TaskNode(void) {}

double TaskNode::GetExpectedExecTime(void) 
{
  PRINT("TaskNode::GetExpectedExecTime\n");

  double survival_prob = 1 - failure_prob_;
  effective_expected_exec_time_ = 0.0;
  // Check whether current task preserved or not

  if(is_preserved_) {

    for(int i=0; i<MAX_FAULTS; i++) {
      effective_expected_exec_time_ += pow(failure_prob_, i) * survival_prob * ( (exec_time_ + restore_time_) * i + exec_time_);
    }

    expected_exec_time_ = preserve_time_ + effective_expected_exec_time_ + (*parent_list_.begin())->expected_exec_time_;

  }
  else {

    if(parent_list_.size() == 1) {

      for(int i=0; i<MAX_FAULTS; i++) {
        effective_expected_exec_time_ += pow(failure_prob_, i) * survival_prob * ( (exec_time_ + (*parent_list_.begin())->expected_exec_time_) * i + exec_time_);
      }

      expected_exec_time_ = effective_expected_exec_time_ + (*parent_list_.begin())->expected_exec_time_;

    }

    else if(parent_list_.size() > 1) {
      // Search for max value among predecessors
      auto it=parent_list_.begin();
      double max_pred_expected_exec_time = (*it)->expected_exec_time_;
      double max_pred_effective_expected_exec_time_ = (*it)->effective_expected_exec_time_;
      for(; it!=parent_list_.end(); ++it) {
        if((*it)->expected_exec_time_ > max_pred_expected_exec_time) {
          max_pred_expected_exec_time = (*it)->expected_exec_time_;
        }

        if((*it)->effective_expected_exec_time_ > max_pred_effective_expected_exec_time_) {
          max_pred_effective_expected_exec_time_ = (*it)->effective_expected_exec_time_;
        }
      }

      for(int i=0; i<MAX_FAULTS; i++) {
        effective_expected_exec_time_ += pow(failure_prob_, i) * survival_prob * ( (exec_time_ + max_pred_effective_expected_exec_time_) * i + exec_time_);
      }
      
      expected_exec_time_ = effective_expected_exec_time_ + max_pred_expected_exec_time;
    }
  }

  return expected_exec_time_;
}


double TaskNode::GetExpectedEnergy(void) 
{
  PRINT("TaskNode::GetExpectedEnergy\n");

  double survival_prob = 1 - failure_prob_;
  effective_expected_energy_ = 0.0;

  if(is_preserved_) { // with preservation case

    for(int i=0; i<MAX_FAULTS; i++) {
      effective_expected_energy_ += pow(failure_prob_, i) * survival_prob * ( (energy_ + restore_energy_) * i + energy_);
    }

    expected_energy_ = preserve_energy_ + effective_expected_energy_ + (*parent_list_.begin())->expected_energy_;
  }
  else {

    if(parent_list_.size() == 1) {  // without-preservation case
      expected_energy_ = 0;
      for(int i=0; i<MAX_FAULTS; i++) {
        effective_expected_energy_ += pow(failure_prob_, i) * survival_prob * ( (energy_ + (*parent_list_.begin())->expected_energy_) * i + energy_);
      }
      expected_energy_ = effective_expected_energy_ + (*parent_list_.begin())->expected_energy_;
    }
    else if(parent_list_.size() > 1) { // Multi-predecessor case
      // Search for max value among predecessors
      double sum_pred_expected_energy = 0.0;
      double sum_pred_effective_expected_energy = 0.0;
      for(auto it=parent_list_.begin(); it!=parent_list_.end(); ++it) {
        sum_pred_expected_energy += (*it)->expected_energy_;
        sum_pred_effective_expected_energy = (*it)->effective_expected_energy_;
      }

      for(int i=0; i<MAX_FAULTS; i++) {
        effective_expected_energy_ += pow(failure_prob_, i) * survival_prob * ( (energy_ + sum_pred_effective_expected_energy) * i + energy_);
      }

      expected_energy_ = effective_expected_energy_ + sum_pred_expected_energy;
    }
    
  }

  return expected_energy_;

}




void TaskNode::PrintInfo(void)
{
  PRINT("[%lu]   Expected exec_time : %f\n", uid_, expected_exec_time_);
  PRINT("[%ld]   Eff Exp  exec_time : %f (orig %f)\n", sweep_id_, effective_expected_exec_time_, exec_time_);
  PRINT("[%lu]  Expected energy    : %f\n", sweep_mask_, expected_energy_);
  PRINT("      Eff Exp  energy    : %f (orig %f)\n", effective_expected_energy_, energy_);
  PRINT("      Failure Prob       : %f\n", failure_prob_);
  PRINT("      Bandwidth          : %f\n", bandwidth_);
  PRINT("      Energy per Bypte   : %f\n", energy_per_byte_);
  PRINT("      Data Volume        : %f\n", data_volume_);
  PRINT("      Preserve Time      : %f\n", preserve_time_);
  PRINT("      Restore  Time      : %f\n", restore_time_);
  PRINT("      Preserve Energy    : %f\n", preserve_energy_);
  PRINT("      Restore  Energy    : %f\n", restore_energy_);
  cout << "iter : " << LegionDAG::curr_iter_ 
       << "\tmask : " << sweep_mask_ 
       << "\tid   : " << sweep_id_
       << "\tprv  : " << is_preserved_ 
       << "\tprv #: " << LegionDAG::curr_prv_num_ 
       << "\n=====================================" << endl; 
}
