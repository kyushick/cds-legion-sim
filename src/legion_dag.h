#include <iostream>
#include <string>
#include <random>
#include <list>
#include <map>
#include <vector>
#include <functional>
#include "define.h"

using namespace std;

/*class NodeInfo {
public:
  double expected_exec_time_;
  double effective_expected_exec_time_;
  double expected_energy_;
  double effective_expected_energy_;

  double preserve_time_;
  double restore_time_;
  double failure_prob_;

public:
  NodeInfo(void) {
    failre_prob_   = DEFAULT_FAILURE_PROBABILITY; 
    preserve_time_ = DEFAULT_PRESERVE_TIME;
    restore_time_  = DEFAULT_RESTORE_TIME;
  }

  NodeInfo(const double &failre_prob, const double &preserve_time, const double &restore_time) {
    failre_prob_   = failre_prob; 
    preserve_time_ = preserve_time;
    restore_time_  = restore_time;
  }

  ~NodeInfo() {}

};*/



/**@class TaskNode
 * @brief This object represents each node in DAG.
 * 
 * current_task_node.create_child(new TaskNode)
 * 
 */
class TaskNode {
  friend class LegionDAG;
  static random_device generator_;
  uniform_real_distribution<double> distribution_;

  unsigned long uid_;
  unsigned long sweep_id_;
  unsigned long sweep_mask_;
  bool erased_; // For topological sort 

  list<TaskNode *> parent_list_;
  list<TaskNode *> children_list_;

  double expected_exec_time_;
  double effective_expected_exec_time_;
  double expected_energy_;
  double effective_expected_energy_;

  // This is the final decision for resilience.
  bool   is_preserved_;
  bool   is_reliable_;

  // Machine-specific information
  double failure_rate_;
  double bandwidth_;
  double energy_per_byte_;

  // App-specific information
  double data_volume_;
  double exec_time_;
  double energy_;

  // Depends on preservation volume and medium (BW).
  // They can be calculated by above information.
  // Assumed that energy/Byte is constant per medium.
  // Therefore, if volume to preserve is given,
  // total energy for preservation can be calculated.
  double preserve_time_;
  double restore_time_;
  double preserve_energy_;
  double restore_energy_;
  double failure_prob_;

public:
  TaskNode(const unsigned long &uid);
  TaskNode(const unsigned long &uid, const long &sweep_id) ;
  ~TaskNode(void); 

  void AddChildNode(TaskNode *child) 
  {
    // Link new child to the current node.
    children_list_.push_back(child);

    // Link the current node to new child.
    child->parent_list_.push_back(this);
  }

  void SetExecTime(double execution_time)
  {
    exec_time_ = execution_time;
  }

  unsigned long uid(void) 
  { return uid_; }

  double exec_time(void) 
  { return exec_time_; }

  double energy(void) 
  { return energy_; }
  
  // Throw a dice to determine failure or not
  bool IsFailed(void) 
  {
    distribution_.reset();
    double error = distribution_(generator_);
    return error < failure_prob_;
  }

  void Sweep(void);

  double GetExpectedExecTime(void);

  double GetExpectedEnergy(void);

  void PrintInfo(void);
};

struct ResultObj {
  double time_;
  double energy_;
  map<unsigned long, double> topo_map_;

  ResultObj(void) 
  {
    time_ = 0.0;
    energy_ = 0.0;
  }
  ~ResultObj(void) {}
};

/**@class LegionDAG
 * @brief LegionDAG is a data structure for DAG of Legion Tasks.
 * 
 * Legion DAG is the important data structure of simulator for Legion resiliency.
 * Simulator traverses Legion DAG from the initial node to the leaf calculating
 * expected execution time (performance efficiency) and expected energy.
 *
 */
class LegionDAG {
  friend class TaskNode;
  // Initial node for DAG
  TaskNode *root_;
  map<unsigned long, TaskNode *> task_map_;

  static map<unsigned long, unsigned long> pred_counter_;
  static list<TaskNode *> topo_list_;

  // Results containers
  map<unsigned long, ResultObj *> sweep_map_;
  map<unsigned long, vector<ResultObj *>> prv_num_map_;

  static unsigned long curr_iter_;
  static unsigned long curr_prv_num_;
  unsigned long iter_;
  static unsigned long sweep_id_gen_;
public:
  LegionDAG(void);  
  ~LegionDAG(void);  
  
  void CreateDAG(const char *filename);
  void SetIter(void);
  void InitSweepID(void);
  void Init(void);
  void RecordResult(ResultObj *result);
  void DumpResult(const char *filepath);
  void RunEstimation(void);
  void RunSimulation(void);
  void TraverseDAG(const function<void(TaskNode*)> &Op);
  void TopologicalSort(void); 
  void TopologicalSort(TaskNode *target);
  static void CalcNodeInfo(TaskNode *target);
  static void SetSweepID(TaskNode *target);
  static void SetSweepMask(TaskNode *target);
  static void Estimate(TaskNode *target);
  static void PrintTime(TaskNode *target);
  static void PrintEnergy(TaskNode *target);
  static void PrintInfo(TaskNode *target);

};

